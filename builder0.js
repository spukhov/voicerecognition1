console.log('11');
var recognition = new webkitSpeechRecognition();
recognition.continuous = true;
recognition.lang = 'en-US';

var words = [];
var currentQuestionIndex = 0;
var questions = [];
var finished = false;

var questionnaire = {
  name: 'test app',
  sections : [
    {
      name : 'section 1',
      questions: [
        {
          title : 'What is the name of new application?',
          result : ''
        },
        {
          title : 'How many sections should it have?',
          result : ''
        }
      ]
    },
    {
      name : 'section 2',
      questions: [
        {
          title : 'What is the name of section 1?',
          result : ''
        },
        {
          title : 'How many questions should it have?',
          result : ''
        },
        {
          title : 'What is the label of the 1st field in section 1?',
          result : ''
        }
      ]
    }
  ]
};

function initQuestions () {
  // iterate through all sections
  for (var i = 0; i < questionnaire.sections.length; i++) {
      for (var j = 0; j < questionnaire.sections[i].questions.length; j++) {
        var question = questionnaire.sections[i].questions[j];
        questions.push(question);
      }
  }
  console.log(questions.length + ' questions initialized');
}

function triggerNextQuestion() {
  if (currentQuestionIndex >= questions.length) {
    console.log('finished questionnaire');
    finished = true;
    return;
  }
  var q = questions[currentQuestionIndex];
  showHelp(q.title);
  console.log(q.title);
}

function fillCurrentQuestion(answer) {
  if (!finished) {
    var q = questions[currentQuestionIndex];
    q.result = answer;
    $('.content').append('<p>' + q.result + '</p>')
    currentQuestionIndex++;
  }
}

recognition.onstart = function() {
    console.log('start');
    initQuestions();
    triggerNextQuestion();
}

recognition.onresult = function(event) {
  console.log(event);
  if (!finished) {
    var answer;
    var speechRecognitionResult = event.results[event.resultIndex];
    var results = [];
    for (var k = 0; k < speechRecognitionResult.length; k++) {
      results[k] = speechRecognitionResult[k].transcript;
    }
    console.error(results.toString());
    parseResult(results.toString())
    fillCurrentQuestion(results.toString());
    triggerNextQuestion();
  }

};

function printResults() {
  var resultStr = '';
  for (var i = 0; i < questions.length; i++) {
    resultStr += 'question: ' + questions[i].title + '\n' +
      'answer: ' + questions[i].result + '\n' +
      '=====================' + '\n';
  }
  $('.content').text(resultStr);
}



function parseResult(result) {
  switch (currentQuestionIndex) {
    case 0:
      createQuestionnaire(result);
      break;
    case 1:
      initSectionsByNumber(result);
      break;
    case 2:
      setNameForSectionOne(result);
      break;
    case 3:
      initQuestionsByNumber(result);
      break;
    case 4:
      setNameForFieldOne(result);
      break;
  }

}

function createQuestionnaire (name) {
  $('#questionnaire > .panel-heading').text('Questionnaire: ' + name);
  $('#questionnaire').show();
}

function initSectionsByNumber (number) {
  sectionsCount = parseInt(number);
  for (var i = 0; i < sectionsCount; i++) {
    $('#questionnaire > .panel-body').append('<div class="panel panel-info section">' +
            '<div class="panel-heading">Section ' + (i+1) +'</div>' +
            '<div class="panel-body"></div></div>');
  }
}

function initQuestionsByNumber (number) {
  var questionsCount = parseInt(number);
  for (var i = 0; i < questionsCount; i++) {
    $('.section:first > .panel-body').append('<div class="form-group">'+
                  '<label for="exampleInputEmail1">Field ' + (i+1) + '</label>' +
                  '<input type="text" class="form-control" id="exampleInputEmail1"></div>');
  }
}

function setNameForSectionOne (name) {
  $('.section:first .panel-heading').text(name);
}

function setNameForFieldOne (name) {
  $('.section:first label:first').text(name);
  showHelp('Thank you!');
}

function showHelp(str) {
  $('#helper > h3').text(str);
}

recognition.start();
